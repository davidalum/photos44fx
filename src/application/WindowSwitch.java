package application;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class WindowSwitch {

public Stage mainStage;

WindowSwitch(Stage primary){
	mainStage = primary;	
}

public void switchScene(String newScene) throws IOException{

	FXMLLoader loader = new FXMLLoader();

	if(newScene.equals("admin")){
		loader.setLocation(getClass().getResource("Admin.fxml")); 
		Admin admin = loader.getController();
	}
	else if(newScene.equals("login")){
		loader.setLocation(getClass().getResource("Login.fxml")); 
		Login log = loader.getController();
	}
	else if(newScene.equals("nonadmin")){
		loader.setLocation(getClass().getResource("NonAdmin.fxml")); 
		NonAdmin nonAdm = loader.getController();
	}
	else if(newScene.equals("searchResults")){
		loader.setLocation(getClass().getResource("SearchResults.fxml")); 
		SearchResults searchRes = loader.getController();
		Pane root = (Pane)loader.load();
		Scene scene = new Scene(root,600,472);
		mainStage.setScene(scene);
		mainStage.show();
		return;
	}
	else if(newScene.equals("caption")){
		loader.setLocation(getClass().getResource("CaptionEntry.fxml")); 
		Caption captCont = loader.getController();
	}
	else if(newScene.equals("display")){
		loader.setLocation(getClass().getResource("Display.fxml")); 
		Display dispCont = loader.getController();
	}
	else if(newScene.equals("fileEntry")){
		loader.setLocation(getClass().getResource("FileEntry.fxml")); 
		FileEntry fileCont = loader.getController();
	}

	else if(newScene.equals("search")){
		loader.setLocation(getClass().getResource("Search.fxml")); 
		Search searchCont = loader.getController();
	}
	else if(newScene.equals("slideshow")){
		loader.setLocation(getClass().getResource("SlideShow.fxml")); 
		SlideShow ssCont = loader.getController();
	}
	else if(newScene.equals("thumbnail")){
		loader.setLocation(getClass().getResource("Thumbnail.fxml")); 
		Thumbnail thumbCont = loader.getController();
		Pane root = (Pane)loader.load();
		Scene scene = new Scene(root,600,472);
		mainStage.setScene(scene);
		mainStage.show();
		return;
	}
	
	else{
		return;
	}
	
	Pane newRoot = (Pane)loader.load();
	Scene scene = new Scene(newRoot,600,400);
    mainStage.setScene(scene);
    mainStage.show();
   }
}
