package application;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

/**
 * 
 * This is the controller for the Non-Administrator screen
 * The screen most users will interact with.
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */
public class NonAdmin {
	@FXML ListView<String> albumList; @FXML TextField numberphotos; @FXML TextField daterange;
	@FXML Button createAlbumButton; @FXML Button renameAlbumButton; @FXML Button deleteAlbumButton; @FXML Button openAlbumButton;
	@FXML TextField albumNameEntry; @FXML Button searchButton; @FXML Button logoutButton; @FXML Button show;
	private ObservableList<String> obsList=FXCollections.observableArrayList();
	WindowSwitch s=new WindowSwitch(Main.mainStage);
	boolean albumListCreated= false;
	
	/**
	 * Method to populate the albums list
	 * 
	 * @param e FXML paramter 
	 * @throws IOException
	 */
	public void start(ActionEvent e)throws IOException{
		Button click = (Button)e.getSource();
		if(click==show && albumListCreated==false) {
			int index = Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.size();

			albumList.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal)->showItem());
			for(int i=0; i < index; i++){
				if (!obsList.contains((Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.get(i).n))){
					obsList.add(Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.get(i).n);
					albumList.setItems(obsList);
				}
			}
		albumList.getSelectionModel().clearSelection();
		albumList.getSelectionModel().select(0);
		albumListCreated = true;
	  }	
	}
	/**
	 * Used to logout of non admin user 
	 * 
	 * @param e FXML parameter
	 * @throws IOException
	 */
	public void logout(ActionEvent e) throws IOException{
		s.switchScene("login");
		Main.activeUser = null;	
	}
	
	/**
	 * 
	 * @param e FXML paramter
	 * @throws IOException 
	 */
	public void search(ActionEvent e)throws IOException{
		s.switchScene("search");
	}
	
	/**
	 * Allows user to either create, rename, or delete an album.
	 * 
	 * @param e FXML paramter 
	 * @throws IOException
	 */
	public void startNonAdmin(ActionEvent e) throws IOException{
		Button click=(Button)e.getSource();
		//Open album button click
		if(click == openAlbumButton) {
			int index = albumList.getSelectionModel().getSelectedIndex();
			if(index == -1){
				ErrorDisplay(1);
				return;
			}
			Main.activeAlbum = Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.get(Main.userData.get(Main.getUserIndex(Main.activeUser)).getAlbumIndex(obsList.get(index)));
			s.switchScene("thumbnail");
		}
		
		else if(click == createAlbumButton) {
			String albumName = albumNameEntry.getText();

			if (albumName.equals("")) {
				ErrorDisplay(4);
				return;
			}
			if ((obsList.contains(albumName))){
				
				ErrorDisplay(2);
				return;
			}
			else{
				
				Album classic=new Album(albumName);
				Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.add(classic);
				obsList.add(albumName);
				albumList.setItems(obsList);
				albumList.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) ->
				showItem());
				albumList.getSelectionModel().select(0);
			}
			
		}
		else if(click == renameAlbumButton) {
			if (albumList.getSelectionModel().getSelectedIndex()==-1 || albumNameEntry.getText().equals("")) {
				ErrorDisplay(3);
				return;				
			}
			else {
				String albumName = albumNameEntry.getText();
				Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.get(albumList.getSelectionModel().getSelectedIndex()).n=albumName;
				obsList.add(albumName);
				obsList.remove(albumList.getSelectionModel().getSelectedIndex());
				albumList.setItems(obsList);
				
			}
			
		}
		if(click == deleteAlbumButton) {
			int index = albumList.getSelectionModel().getSelectedIndex();
			if (index == -1) {
				ErrorDisplay(3);
				return;
			}
			Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.remove(albumList.getSelectionModel().getSelectedIndex());
			obsList.remove(albumList.getSelectionModel().getSelectedIndex());
		}
		
	}
	
	/**
	 * Used to display error messages
	 * 
	 * @param i Variable used to indicate which error to display
	 */
	
	public void ErrorDisplay(int i){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText("Error: Please Try Again");
		//Open Album Error 1
		if(i == 1) {
			alert.setContentText("Select Album and Try Again");
		}
		//Create Album Error, Album already exists
		else if(i==2) {
			alert.setContentText("Fill in the Album Name ");
		}
		//Delete Album and Ren Album error
		else if(i==3) {
			alert.setContentText("Fill in a Valid Album Name");
		}
		//Create Album Error, no n given
		else if(i==4) {
			alert.setContentText("Fill in an Album Name");
		}
		alert.showAndWait();
	}
	
	/**
	 * Allows user to select an album for modification
	 */
	private void showItem(){
		if(!obsList.isEmpty()){
			int index=albumList.getSelectionModel().getSelectedIndex();
			if(index!=-1){
				Album temp = Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.get(Main.userData.get(Main.getUserIndex(Main.activeUser)).getAlbumIndex(obsList.get(index)));
				numberphotos.setText((""+temp.totalPhotos));
				daterange.setText(""+temp.getDateRange());
			}
		}
	}	
	
}
