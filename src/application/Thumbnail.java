package application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;



public class Thumbnail {
	@FXML Button addPhotoButton; @FXML Button captionButton; @FXML Button removePhotoButton; @FXML Button copyButton;
	@FXML Button moveButton; @FXML Button displayButton; @FXML Button slideshowButton; @FXML Button returnMainButton;
	@FXML ChoiceBox destinationAlbum; @FXML TilePane thumbnailDisplay; @FXML Button prevThumb; @FXML Button nextThumb;
	@FXML ImageView img1; @FXML ImageView img2; @FXML ImageView img3; @FXML ImageView img4; @FXML ImageView img5;
	@FXML ArrayList<ImageView> imgList; @FXML ArrayList<Label> captionList;

	private ObservableList<String> obsList=FXCollections.observableArrayList();
	WindowSwitch window=new WindowSwitch(Main.mainStage);
	int thumbnailIndex=0;
	int selectedImageIndex=-1;
	ImageView clicked=null;
	
	public void initiate(MouseEvent e) throws Exception{
		if(img1.getImage()==null){
			for(int i=0; i<5;i++){
				imgList.get(i).setImage(null);
				captionList.get(i).setText("");
				if(i+thumbnailIndex<Main.activeAlbum.totalPhotos){
					Image temp = new Image("file:"+Main.activeAlbum.photos.get(i+thumbnailIndex).address);
					imgList.get(i).setImage(temp);
					captionList.get(i).setText(Main.activeAlbum.photos.get(i+thumbnailIndex).caption);
				}
			}
			ArrayList<Album> temp=Main.userData.get(Main.getUserIndex(Main.activeUser)).albums;
			for(int i=0;i<temp.size();i++){
				if(!temp.get(i).equals(Main.activeAlbum)){
					obsList.add(temp.get(i).n);
				}
			}
			destinationAlbum.setItems(obsList);
		}
	}
	public void start(ActionEvent e) throws IOException{
		Button click=(Button)e.getSource();
		
		if (click == addPhotoButton){
			window.switchScene("fileEntry");
			selectedImageIndex=-1;
			clicked=null;
		}
		
		else if (click == captionButton){
			if(selectedImageIndex!=-1){
				Main.activePhoto=Main.activeAlbum.photos.get(selectedImageIndex);
				window.switchScene("caption");
			}
			else{
			ErrorDisplay(1);
				return;
			}
		}
		
		else if (click == removePhotoButton){
			if(selectedImageIndex!=-1){
				Main.activeAlbum.removePhoto(selectedImageIndex);
				clicked.setOpacity(1);
				clicked=null;
				selectedImageIndex=-1;
			if(thumbnailIndex==Main.activeAlbum.totalPhotos && thumbnailIndex!=0){
				thumbnailIndex-=5;
			}
			for(int i=0; i<5;i++){
				imgList.get(i).setImage(null);
				captionList.get(i).setText("");
				if(i+thumbnailIndex<Main.activeAlbum.totalPhotos){
					Image temp = new Image("file:"+Main.activeAlbum.photos.get(i+thumbnailIndex).address);
					imgList.get(i).setImage(temp);
					captionList.get(i).setText(Main.activeAlbum.photos.get(i+thumbnailIndex).caption);
				}
			}
			}
			else{
				ErrorDisplay(2);
				return;
			}
		//Remove Selected Photo
		}
		
		else if (click == copyButton){
			if(selectedImageIndex!=-1){
				if(destinationAlbum.getSelectionModel().getSelectedIndex()==-1){
					ErrorDisplay(3);
					return;
				}
				else{
					Album dest= Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.get(Main.userData.get(Main.getUserIndex(Main.activeUser)).getAlbumIndex(obsList.get(destinationAlbum.getSelectionModel().getSelectedIndex())));
					if(dest.contains(Main.activeAlbum.photos.get(selectedImageIndex).address)){
						ErrorDisplay(4);
						return;
					}
					else{
						dest.addPhoto(Main.activeAlbum.photos.get(selectedImageIndex));
						clicked.setOpacity(1);
						clicked=null;
						selectedImageIndex=-1;
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Success");
						alert.setHeaderText("Success");
						alert.setContentText("Photo Copied Successfully");
						alert.showAndWait();
						return;
					}
				  }
			    }
				else{
					ErrorDisplay(5);
					return;
				}
		//copy selected photo to ChoiceBox destinationAlbum
		}
		
		else if(click == moveButton){
			if(selectedImageIndex!=-1){
				if(destinationAlbum.getSelectionModel().getSelectedIndex()==-1){
					ErrorDisplay(6);
					return;
				}
				else{
					Album dest= Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.get(Main.userData.get(Main.getUserIndex(Main.activeUser)).getAlbumIndex(obsList.get(destinationAlbum.getSelectionModel().getSelectedIndex())));
					if(dest.contains(Main.activeAlbum.photos.get(selectedImageIndex).address)){
						ErrorDisplay(7);
						return;
					}
					else{
					dest.addPhoto(Main.activeAlbum.removePhoto(selectedImageIndex));
					clicked.setOpacity(1);
					clicked=null;
					selectedImageIndex=-1;
				
					if(thumbnailIndex==Main.activeAlbum.totalPhotos && thumbnailIndex!=0){
					thumbnailIndex-=5;
				}
					for(int i=0; i<5;i++){
					imgList.get(i).setImage(null);
					captionList.get(i).setText("");
					if(i+thumbnailIndex<Main.activeAlbum.totalPhotos){
						Image temp = new Image("file:"+Main.activeAlbum.photos.get(i+thumbnailIndex).address);
						imgList.get(i).setImage(temp);
						captionList.get(i).setText(Main.activeAlbum.photos.get(i+thumbnailIndex).caption);
					}
				 }
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Success");
				alert.setHeaderText("Success");
				alert.setContentText("Photo Moved Successfully");
				alert.showAndWait();
				return;
				}
			  }
			}
			else{
				ErrorDisplay(5);
				return;
			}
			//Move selected photo to ChoiceBox destinationAlbum
		}
		
		else if ( click == displayButton){
			if(selectedImageIndex!=-1){
				Main.activePhoto=Main.activeAlbum.photos.get(selectedImageIndex);
				window.switchScene("display");
			}
			else{
				ErrorDisplay(5);
				return;
		    }
		}
		
		else if(click == slideshowButton){
			window.switchScene("slideshow");
		}
		
		else if(click == returnMainButton){
			window.switchScene("nonadmin");
			Main.activeAlbum=null;
			if(clicked!=null){
				selectedImageIndex=-1;
				clicked.setOpacity(1);
				clicked=null;
			}
		}
		
		else if (click == nextThumb){
			if(thumbnailIndex+5<Main.activeAlbum.totalPhotos){
				thumbnailIndex+=5;
				if(clicked!=null){
					selectedImageIndex=-1;
					clicked.setOpacity(1);
					clicked=null;
				}
				for(int i=0; i<5;i++){
					imgList.get(i).setImage(null);
					captionList.get(i).setText("");
					if(i+thumbnailIndex<Main.activeAlbum.totalPhotos){
						Image temp = new Image("file:"+Main.activeAlbum.photos.get(i+thumbnailIndex).address);
						imgList.get(i).setImage(temp);
						captionList.get(i).setText(Main.activeAlbum.photos.get(i+thumbnailIndex).caption);
					}
			    }
		  }
		}
		else if (click == prevThumb){
			if(thumbnailIndex>0){
			thumbnailIndex-=5;
			if(clicked!=null){
				selectedImageIndex=-1;
				clicked.setOpacity(1);
				clicked=null;
			}
			for(int i=0; i<5;i++){
				imgList.get(i).setImage(null);
				captionList.get(i).setText("");
				if(i+thumbnailIndex<Main.activeAlbum.totalPhotos){
					Image temp = new Image("file:"+Main.activeAlbum.photos.get(i+thumbnailIndex).address);
					imgList.get(i).setImage(temp);
					captionList.get(i).setText(Main.activeAlbum.photos.get(i+thumbnailIndex).caption);
				}
			 }
			}
		  }
		} 
	
	/**
	 * Used to display error messages
	 * 
	 * @param i Variable used to indicate which error to display
	 */
		public void ErrorDisplay(int i){
			
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		if(i == 1) {
		alert.setHeaderText("No image selected");
		alert.setContentText("Select image and try again");
		}
		else if(i==2) {
		alert.setHeaderText("No image selected");
		alert.setContentText("Select image and try again");
		}
		else if(i==3) {
		alert.setHeaderText("No destination selected");
		alert.setContentText("Select destination and try again");
		}
		else if(i==4) {
		alert.setHeaderText("Destination already has this photo");
		alert.setContentText("Choose different photo or different destination.");
		}
		else if(i == 5) {
		alert.setHeaderText("No image selected");
		alert.setContentText("Select image and try again");
		}
		else if(i ==6) {
		alert.setHeaderText("No destination selected");
		alert.setContentText("Select destination and try again");
		}
		else if(i ==7) {
		alert.setHeaderText("Destination already has this photo");
		alert.setContentText("Choose different photo or different destination.");
		}
		alert.showAndWait();
		}

	public void imageClick(MouseEvent e) throws Exception{
		if(clicked!=null){
			clicked.setOpacity(1);
		}
		clicked = (ImageView)e.getTarget();
		clicked.setOpacity(0.5);
		int imageIndex = (int)clicked.getId().charAt(3)-49+thumbnailIndex;
		selectedImageIndex=imageIndex;
	}
}
