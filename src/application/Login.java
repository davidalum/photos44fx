package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
/**
 *
 * This is the controller for the Login screen. 
 * Allwos the user to either login as a regular, non-admin user or as admin
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */
 
 
public class Login {
	@FXML Button login; @FXML TextField errorText; @FXML TextField usernameBox;
	WindowSwitch s=new WindowSwitch(Main.mainStage);

	/**
	 * Determines whether the user is an admin or non-admin
	 * 
	 * @param e  FXML paramater 
	 * @throws IOException
	 */
	public void start(ActionEvent e) throws IOException{
		Button click=(Button)e.getSource();
		if(click == login){
			if(usernameBox.getText().equals("admin")){
				s.switchScene("admin");
			}
			else if(Main.users.contains(usernameBox.getText())) {
				Main.activeUser=usernameBox.getText();
				s.switchScene("nonadmin");
			}
			else{			
				errorText.setText("Invalid Username");
			}
		}
	}
}
