package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;


/**
 * Slideshow funcitonality, allowing viewing of photos as a slideshow
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */
public class SlideShow{
	@FXML Button previousButton; @FXML Button nextButton; @FXML Button exitButton; 
	@FXML ImageView focusImage;
	WindowSwitch s=new WindowSwitch(Main.mainStage);
	int currentIndex=0;
	
	/**
	 * Intiates the slideshow screen
	 * 
	 * @param e FXML parameter
	 * @throws Exception
	 */
	public void initiate(MouseEvent e) throws Exception{
		if(focusImage.getImage()==null){
			Image temp = new Image("file:"+Main.activeAlbum.photos.get(currentIndex).address);
			focusImage.setImage(temp);
		}
	}
	/**
	 * Allows user to either move forward or reverse through the slideshow. Also allows exit. 
	 * 
	 * @param e FXML paramter
	 * @throws IOException
	 */
	public void start(ActionEvent e) throws IOException{
		//switches displayed photo to previous in list
		Button click=(Button)e.getSource();
		if (click == previousButton) {
			if(currentIndex==0 && Main.activeAlbum.totalPhotos>1){
				currentIndex=Main.activeAlbum.totalPhotos-1;
				Image temp = new Image("file:"+Main.activeAlbum.photos.get(currentIndex).address);
				focusImage.setImage(temp);
			}else if(Main.activeAlbum.totalPhotos>1){
				currentIndex--;
				Image temp = new Image("file:"+Main.activeAlbum.photos.get(currentIndex).address);
				focusImage.setImage(temp);
			}
			
		}
		else if(click == nextButton) {
			if(currentIndex==Main.activeAlbum.totalPhotos-1 && Main.activeAlbum.totalPhotos>1){
				currentIndex=0;
				Image temp = new Image("file:"+Main.activeAlbum.photos.get(currentIndex).address);
				focusImage.setImage(temp);
			}else if(Main.activeAlbum.totalPhotos>1){
				currentIndex++;
				Image temp = new Image("file:"+Main.activeAlbum.photos.get(currentIndex).address);
				focusImage.setImage(temp);
			}
		}
		else if(click == exitButton) {
			s.switchScene("thumbnail");
			
		}
		
	}
	
}