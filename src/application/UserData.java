//Written by David and Ani
package application;

import java.io.Serializable;
import java.util.ArrayList;

public class UserData implements Serializable{
	String username;
	ArrayList<Album> albums = new ArrayList<Album>();
	
	UserData(String username){
		this.username=username;
	}
	
	public int getAlbumIndex(String albumName){
		for(int i=0;i<albums.size();i++){
			if(albums.get(i).n.equals(albumName)){
				return i;
			}
		}
		return -1;
	}
}
