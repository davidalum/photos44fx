package application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * Shows information regarding the photo as well as tagging capabilities. 
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */
public class Display {
	@FXML Button addTagButton; @FXML Button deleteTagButton; @FXML Button returnThumbnailButton;
	@FXML TextField captionDisplay; @FXML TextField dateDisplay; @FXML TextField tagEntryBox;
	@FXML ListView tagsList; @FXML ImageView focusPicture;
	
	private ObservableList<String> obsList=FXCollections.observableArrayList();
	WindowSwitch s=new WindowSwitch(Main.mainStage);
	
	/**
	 * Initiates the screen and shows the photo
	 * 
	 * @param e  FXML paramater
	 * @throws Exception
	 */
	public void initiate(MouseEvent e) throws Exception{
			Image temp = new Image("file:"+Main.activePhoto.address);
			focusPicture.setImage(temp);
			captionDisplay.setText(Main.activePhoto.getcaption());
			dateDisplay.setText(Main.activePhoto.getDate());
			obsList.clear();
			for(int i=0;i<Main.activePhoto.tags.size();i++){
				obsList.add(Main.activePhoto.tags.get(i).tagname +":"+ Main.activePhoto.tags.get(i).tagvalue);
			}
			tagsList.setItems(obsList);
	}
	
	/**
	 * Method to determine what actiont to take regarding the tags of a photo.
	 * Can either add or delete a tag.
	 * 
	 * @param e  FXML parameter
	 * @throws IOException
	 */
	public void action(ActionEvent e) throws IOException{
		Button click=(Button)e.getSource();
		if(click == addTagButton) {
		String inputTag = tagEntryBox.getText();
		if(Main.activePhoto.addTag(inputTag)){
			ArrayList<Tag> temp=Main.activePhoto.tags;
			obsList.add(temp.get(temp.size()-1).tagname +":"+ temp.get(temp.size()-1).tagvalue);
		}
		else{
			ErrorDisplay(1);
			return;
		}
	}
	
		else if(click == deleteTagButton){
		int temp=tagsList.getSelectionModel().getSelectedIndex();
		
		if(temp==-1){
			ErrorDisplay(2);
			return;
		}
		else{
			String tagStr=obsList.get(temp);
			List<String> tagList = Arrays.asList(tagStr.split(":"));
			String tagname=tagList.get(0);
			String tagvalue=tagList.get(1);
			for(int i=0;i<Main.activePhoto.tags.size();i++){
				if(Main.activePhoto.tags.get(i).tagname.equals(tagname)&&Main.activePhoto.tags.get(i).tagvalue.equals(tagvalue)){
					Main.activePhoto.tags.remove(i);
					obsList.remove(temp);
					return;
				}
			}
		}
	}
		
	else if(click == returnThumbnailButton){
		s.switchScene("thumbnail");
		Main.activePhoto=null;
	}
  }
	
	/**
	 * Used to display error messages
	 * 
	 * @param i Variable used to indicate which error to display
	 */
	public void ErrorDisplay(int i){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		if(i == 1) {
			alert.setHeaderText("Can't add tag");
			alert.setContentText("Format is incorrect or tag already exists");
		}
		else if(i==2) {
			alert.setHeaderText("Can't delete tag");
			alert.setContentText("Tag not chosen.");
		}
		alert.showAndWait();
	}
	
	
	
}