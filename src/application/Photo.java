package application;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class for photos, and containts all properties of it
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */

public class Photo implements Serializable{
	String address;
	String caption;
	long dateMS;
	ArrayList<Tag> tags = new ArrayList<Tag>();
	
	Photo(String address){
		this.address=address;
		updateDate();
	}
	
	/**
	 * Returns the caption of a photo
	 * 
	 * @return  the caption
	 */
	public String getcaption(){
		return caption;
	}

	/**
	 * Sets the caption for a photo
	 * 
	 * @param caption the caption
	 */
	public void setcaption(String caption){
		this.caption=caption;
	}

	/**
	 * Updates the date for a given photo
	 */
	public void updateDate(){
		File temp = new File(address);
		dateMS=temp.lastModified();
	}

	/**
	 * Gives a user the date of a photo
	 * 
	 * @return the date
	 */
	public String getDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		return sdf.format(dateMS);
	}

	/**
	 * Function to add a tag
	 * 
	 * @param tagString label of tag
	 * @return
	 */
	public boolean addTag(String tagString){
		String tagname;
		String tagvalue;
		List<String> tagList = Arrays.asList(tagString.split(":"));
		if(tagList.size()!=2){
			return false; 
		}
		else{
			tagname=tagList.get(0);
			tagvalue=tagList.get(1);
			for(int i=0;i<tags.size();i++){
				if(tags.get(i).tagname.equals(tagname)&&tags.get(i).tagvalue.equals(tagvalue)){
					return false; 
				}
			}
			tags.add(new Tag(tagname,tagvalue));
			return true;
		}
	}
}
