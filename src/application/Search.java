package application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

/**
 * Controller for search to manage the serach interface. 
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */

public class Search {
	@FXML TextField startDateEntry; @FXML TextField endDateEntry; @FXML TextField tagsEntry;
	@FXML Button searchButton; @FXML Button exitSearchButton; 
	WindowSwitch window=new WindowSwitch(Main.mainStage);

	/**
	 * Allows the user to either search or exit the search screen
	 * 
	 * @param e FXML paramater
	 * @throws IOException
	 */
	public void start(ActionEvent e) throws IOException{
		Button click=(Button)e.getSource();
		
		if(click == exitSearchButton){
			window.switchScene("nonadmin");
		
		}
		
		else if (click == searchButton){
		//checks if inputs are valid and executes a search before opening the next screen
			String tagInput=tagsEntry.getText();
			String startDate = startDateEntry.getText(); 
			String endDate = endDateEntry.getText(); 
			ArrayList<Tag> tagSearch;
			
			
			if(!tagInput.equals("")){
				tagSearch=breakdownTags(tagInput);
				if(tagSearch==null){
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText("Tags are input with incorrect format");
					alert.setContentText("Reformat tags and try again");
					alert.showAndWait();
					return;
				}else{
					ArrayList<Album> albums = Main.userData.get(Main.getUserIndex(Main.activeUser)).albums;
					for(Album i: albums){
						for(Photo j: i.photos){
							for(Tag k: j.tags){
								for(Tag l: tagSearch){
									if(l.tagname.equals(k.tagname) && l.tagvalue.equals(k.tagvalue)){
										Main.searchResults.addPhoto(j);
								}
							}
						}
					}
				}
			}
			window.switchScene("searchResults");
		}
		window.switchScene("searchResults");
	  }
	}	
	
	/**
	 * Allows user to parse tags and inserts them into a list.
	 * @param rawInput
	 * @return
	 */
	public ArrayList<Tag> breakdownTags(String rawInput){
		ArrayList<Tag> searchingTags = new ArrayList<Tag>();
		List<String> tagList = Arrays.asList(rawInput.split(" "));
		for(String i:tagList){
			List<String> temp = Arrays.asList(i.split(":"));
			if(temp.size()!=2){
				return null; //incorrect syntax
			}else{
				searchingTags.add(new Tag(temp.get(0),temp.get(1)));
			}
		}
		return searchingTags;
	}
}