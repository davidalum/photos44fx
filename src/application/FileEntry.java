package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
/**
 * Controller for File entry, allowing user to upload photos
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */
public class FileEntry {
	@FXML Button addPhotoButton; @FXML Button cancelButton; @FXML TextField fileAddress;
	WindowSwitch s=new WindowSwitch(Main.mainStage);

/**
 * Allows controller to decide between either adding a photo or cancelling the addition. 
 * 
 * @param e  FXML paramter
 * @throws IOException
 */
	public void action(ActionEvent e)  throws IOException	{
		Button click=(Button)e.getSource();
		
		if(click ==addPhotoButton) {
		String address= fileAddress.getText();
		File temp = new File(address);
		if(temp.isFile()){
			FileInputStream fis = new FileInputStream(address);
			if(ImageIO.read(fis)==null){
				ErrorDisplay(1);
				return;
			}
			else{
				if(Main.activeAlbum.contains(address)){
					ErrorDisplay(2);
					return;
				}
				else{
					Main.activeAlbum.addPhoto(address);
					s.switchScene("thumbnail");
					return;
				}
			}
		}
		else{
			ErrorDisplay(3);
			return;
		}
	}
		else if (click == cancelButton) {
			s.switchScene("thumbnail");
		}
	}

	
	/**
	 * Used to display error messages
	 * 
	 * @param i Variable used to indicate which error to display
	 */
	public void ErrorDisplay(int i){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		if(i == 1) {
			alert.setHeaderText("File is not a valid Image Type");
			alert.setContentText("Re-enter Address");
		}
		else if(i==2) {
			alert.setHeaderText("Image already exists in Album");
			alert.setContentText("Enter Different Address");
		}
		else if(i==3) {
			alert.setHeaderText("File Doesn't Exist");
			alert.setContentText("Re-enter Address");
		}
		alert.showAndWait();
	}
	
}