package application;
	

import javafx.application.Application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
/**
 * 
 * This is the main. The application begins here. 
 * Multiple ovjects including the main stage, active User/album/photo, 
 * and list of users and user data.
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */

public class Main extends Application {
	public static Stage mainStage;
	public static String activeUser=null;
	public static Album activeAlbum=null;
	public static Photo activePhoto=null;
	
	public static ArrayList<String> users=new ArrayList<String>();
	public static ArrayList<UserData> userData = new ArrayList<UserData>();
	
	public static Album searchResults=new Album("Search Results");
	
	public File userFile= new File("userList.txt");
	public File userDataFile = new File("userDataFile.txt");
	
/**
 * Begins the creation of the app and sets up the first login screen.
 * @param primaryStage Stage for the application
 */
	public void start(Stage primaryStage) {
		try {
			
			FXMLLoader loader = new FXMLLoader();
			mainStage = primaryStage;
			loadStoredInfo();
			
			loader.setLocation(getClass().getResource("login.fxml")); 
			Login log = loader.getController();
			Pane root = (Pane)loader.load();
			Scene scene = new Scene(root,600,400);
			primaryStage.setScene(scene);
			primaryStage.show();
			
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			    public void run(){
			    	Main.activeUser=null;
			    	Main.activeAlbum=null;
			    	Main.activePhoto=null;
			    	PrintWriter writer;
			  
			    	try {
						FileOutputStream fo = new FileOutputStream(userDataFile);
						ObjectOutputStream obo = new ObjectOutputStream(fo);
						for(int i=0; i<userData.size();i++){
							obo.writeObject(userData.get(i));
						}
						obo.writeObject(null);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	   	
			    	try {
						writer = new PrintWriter(userFile);
				    	writer.print("");
						for(int i=0;i<users.size();i++){
							writer.println(users.get(i));
						}
						writer.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					
			    }
			}, "Shutdown-thread")); 
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads all the information stored on file so that data remains persistent
	 * from previous sessions.
	 * 
	 * @throws Exception
	 */
	public void loadStoredInfo() throws Exception{
		BufferedReader fr = new BufferedReader(new FileReader(userFile));
		String username;
		while((username = fr.readLine())!= null){
			users.add(username);
		}
		if(users.isEmpty()){
			buildDefault();
			return;
		}
		
		try {
			FileInputStream fi = new FileInputStream(userDataFile);
			ObjectInputStream obi = new ObjectInputStream(fi);
			UserData tempDat;
			
			while((tempDat=(UserData)obi.readObject())!=null){
				userData.add(tempDat);
			}

		} 
			catch (Exception e) {
			   e.printStackTrace();
		}
	}
	
	/**
	 * The default build for a "stock" user, done intially to create one initial user. 
	 * 
	 */
	public static void buildDefault(){
		users.clear();
		userData.clear();
		users.add("stock");
		UserData stockUser = new UserData("stock");
		Album tempA = new Album("stock photos");
		tempA.addPhoto("./stockPhotos/stockPicture1.jpg");
		tempA.addPhoto("./stockPhotos/stockPicture2.jpg");
		tempA.addPhoto("./stockPhotos/stockPicture3.jpg");
		tempA.addPhoto("./stockPhotos/stockPicture4.jpg");
		tempA.addPhoto("./stockPhotos/stockPicture5.jpg");
		
		stockUser.albums.add(tempA);
		userData.add(stockUser);
	}	
	
	/**
	 * Returns the index of the user in the list. 
	 * @param username
	 * @return
	 */
	public static int getUserIndex(String username){
		for(int i=0; i<userData.size();i++){
			if(userData.get(i).username.equals(username)){
				return i;
			}
		}
		return -1;
	}
	
/**
 * The default main 
 * 
 * @param args  The arguments
 */
	public static void main(String[] args) {
		launch(args);
	}
}



