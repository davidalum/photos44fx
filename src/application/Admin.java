package application;

import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
/**
 * 
 * This is the controller for the Administrator screen, the screen an admin accesses.
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */
public class Admin{
	@FXML ListView<String> userList; @FXML TextField usernameInput; @FXML TextField usernameDisplay; 
	@FXML Button users; @FXML Button create; @FXML Button delete; @FXML Button logout;
	
	private ObservableList<String> obsList=FXCollections.observableArrayList();
	WindowSwitch window = new WindowSwitch(Main.mainStage);

	/**
	 * 
	 * @param e  A paramater from the FXML file
	 * @throws IOException
	 */
	public void startadmin(ActionEvent e) throws IOException{
		Button click=(Button)e.getSource();
		
		if (click == users){
			for (int i=0 ;i < Main.users.size(); i++){
				if (!obsList.contains(Main.users.get(i))){
					obsList.add(Main.users.get(i));
					userList.setItems(obsList);
					userList.getSelectionModel().select(0);
				}
			}
			userList.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) ->
			showItem());
			userList.setItems(obsList);
			userList.getSelectionModel().clearSelection();
			userList.getSelectionModel().select(0);
		}
		
		else if (click==logout){
			window.switchScene("login");
		}

		else if (click == create){
			String user = usernameInput.getText();
			if (user.equals("")){
				ErrorDisplay(1);
				return;
			}
			if ((obsList.contains(user))){
				ErrorDisplay(2);
				return;
			}
			else{
				obsList.add(user);
				Main.users.add(user);
				Main.userData.add(new UserData(user));
				userList.getSelectionModel().select(0);
			}
		}

		else{
			
			if (userList.getSelectionModel().getSelectedIndex()==-1){
				ErrorDisplay(3);
				return;
			}
			String delete = obsList.get(userList.getSelectionModel().getSelectedIndex());
			obsList.remove(userList.getSelectionModel().getSelectedIndex());
			Main.users.remove(delete);
			int index = Main.getUserIndex(delete);
			if(index!=-1){
				Main.userData.remove(index);
			}
		}
	}
	
	/**
	 * Used to display error messages
	 * 
	 * @param i Variable used to indicate which error to display
	 */
	public void ErrorDisplay(int i){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText("Error: Please Try Again");
		if(i == 1) {
			alert.setContentText("Enter a User Name");
		}
		else if(i==2) {
			alert.setHeaderText("User Name already exists");
		}
		else if(i==3) {
			alert.setContentText("Fill in the User Name to Delete");
		}
		else if(i==4) {
			alert.setContentText("Fill in an Album Name");
		}
		alert.showAndWait();
	}
	/**
	 *  Used to select a user on the Admin page. 
	 */
	private void showItem(){
		if(!obsList.isEmpty()){
			int index = userList.getSelectionModel().getSelectedIndex();
			if(index != -1){
				usernameDisplay.setText(obsList.get(index));
			}
		}
	}
}