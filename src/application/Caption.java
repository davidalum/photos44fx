package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * Controller for caption screen, which allows you 
 * you add or change a caption. 
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */
public class Caption {
	@FXML Button confirmButton; @FXML TextField captionEntryBox; @FXML Button cancelButton;
	WindowSwitch window=new WindowSwitch(Main.mainStage);
	
	/**
	 * Gives you the current caption on a photo
	 * 
	 * @param e
	 * @throws Exception
	 */
	public void initiate(MouseEvent e) throws Exception{
		captionEntryBox.setText(Main.activePhoto.getcaption());
	}
	
	
	/**
	 * Method to determine the proper action to be taken with the caption.
	 * Allows you to either confirm the change or cancel.
	 * 
	 * @param e  FXML parameter 
	 * @throws IOException
	 */
	public void action(ActionEvent e) throws IOException{
		Button click=(Button)e.getSource();
		if(click == confirmButton) {
		Main.activePhoto.setcaption(captionEntryBox.getText());
		window.switchScene("thumbnail");
		Main.activePhoto=null;
	}
		else if (click == cancelButton){
		window.switchScene("thumbnail");
		Main.activePhoto=null;
	}
  }
}