package application;

import java.io.IOException;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;

/**
 * Screen to give the user results from a search
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 *
 */
public class SearchResults {
	@FXML Button createAlbumButton; @FXML Button returnSearchButton; @FXML Button returnMainButton; @FXML Button prev; @FXML Button next;
	@FXML TilePane searchResultsPictures; 
	@FXML ArrayList<ImageView> imageList; @FXML ArrayList<Label> captionList;
	@FXML ImageView image1; @FXML ImageView image2; @FXML ImageView image3; @FXML ImageView image4; 
	@FXML ImageView image5; @FXML Label label1; @FXML Label label2; @FXML Label label3; @FXML Label label4; 
	@FXML Label label5; 
	
	WindowSwitch window=new WindowSwitch(Main.mainStage);
	int thumbnailIndex=0;
	
	/**
	 * Iniatilizes the results screen
	 * 
	 * @param e FXML paramater
	 * @throws Exception
	 */
	public void initialize(MouseEvent e) throws Exception{
		if(image1.getImage()==null){
			int i = 0;
			while(i<5) {
				imageList.get(i).setImage(null);
				captionList.get(i).setText("");
				if(i+thumbnailIndex<Main.searchResults.totalPhotos){
					Image temp = new Image("file:"+Main.searchResults.photos.get(i+thumbnailIndex).address);
					imageList.get(i).setImage(temp);
					captionList.get(i).setText(Main.searchResults.photos.get(i+thumbnailIndex).caption);
				}
				i++;
			}
		}
	}
	
	/**
	 * Either allows user to return to non-admin screen or perform the search
	 * 
	 * @param e FXML paramter
	 * @throws IOException
	 */
	public void start(ActionEvent e) throws IOException{
		//creates the album from the search results and returns to nonadmin main menu
		Button click=(Button)e.getSource();

		if(click == createAlbumButton) {
			Main.userData.get(Main.getUserIndex(Main.activeUser)).albums.add(Main.searchResults);
			Main.searchResults=null;
			window.switchScene("nonadmin");
		}
		else if(click == returnSearchButton) {
			window.switchScene("search");
			Main.searchResults=null;			
		}
		else if(click == returnMainButton) {
			window.switchScene("nonadmin");
			Main.searchResults=null;
		}
		else if(click == prev) {
			if(thumbnailIndex>0){
				thumbnailIndex-=5;
				for(int i=0; i<5;i++){
					imageList.get(i).setImage(null);
					captionList.get(i).setText("");
					if(i+thumbnailIndex<Main.searchResults.totalPhotos){
						Image temp = new Image("file:"+Main.searchResults.photos.get(i+thumbnailIndex).address);
						imageList.get(i).setImage(temp);
						captionList.get(i).setText(Main.searchResults.photos.get(i+thumbnailIndex).caption);
					}
				}
			}
			
		}
		else if(click == next) {
			if(thumbnailIndex+5<Main.searchResults.totalPhotos){
				thumbnailIndex+=5;
				for(int i=0; i<5;i++){
					imageList.get(i).setImage(null);
					captionList.get(i).setText("");
					if(i+thumbnailIndex<Main.searchResults.totalPhotos){
						Image temp = new Image("file:"+Main.searchResults.photos.get(i+thumbnailIndex).address);
						imageList.get(i).setImage(temp);
						captionList.get(i).setText(Main.searchResults.photos.get(i+thumbnailIndex).caption);
					}
				}
			}
			
		}
		
	
	
	}
}