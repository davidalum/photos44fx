package application;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
/**
 * This class holds all information regarding an album. 
 * 
 * 
 * @author David Alummoottil
 * @author Anirudh Balachandran
 * @version 1.0
 * @since 2017-11-22
 */
public class Album implements Serializable{
		ArrayList<Photo> photos = new ArrayList<Photo>();
		String n;
		int totalPhotos;
		String dateRange = "";
		
		Album(String n){
			this.n = n;
			totalPhotos = 0;
		}
		
		/**
		 * Adds a photo into the album
		 * 
		 * @param address The address of the photo to insert into an album
		 */
		public void addPhoto(String address){
			photos.add(new Photo(address));
			totalPhotos++;
			updateDates();
		}
		/**
		 * Adds a photo if you are given the photo itself
		 * 
		 * @param photo  a photo item
		 */
		public void addPhoto(Photo photo){
			photos.add(photo);
			totalPhotos++;
			updateDates();
		}
	/**
	 * Removes a photo given the address
	 * 
	 * @param address  the address of the photo
	 */
		public void removePhoto(String address){
			for(int i=0;i<photos.size();i++){
				if(photos.get(i).address.equals(address)){
					photos.remove(i);
					totalPhotos--;
					updateDates();
					return;
				}
			}
		}
	
		/**
		 * Removes a photo based on its index (position in the list)
		 * 
		 * @param position  the index showing the photos location
		 * 
		 */
		public Photo removePhoto(int position){
			totalPhotos--;
			Photo temp = photos.remove(position);
			updateDates();
			return temp;
			
		}

		/**
		 * Indicates whether a photo is in the given album
		 * 
		 * @param address  Address of photo 
		 * @return  boolean value true/false, wheter photo is present
		 */
		public boolean contains(String address){
			for(int i=0;i<photos.size();i++){
				if(photos.get(i).address.equals(address)){
					return true;
				}
			}
			return false;
		}
		/**
		 * Updates the date range for the album
		 */
		public void updateDates(){
			if(!photos.isEmpty()){
				long high=0;
				long low=(long) 100000000000000000000.0;
				for(Photo p: photos){
					if(p.dateMS<low){
						low=p.dateMS;
					}
					if(p.dateMS>high){
						high=p.dateMS;
					}
				}
				
				SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");
				dateRange = date.format(low) +" - "+ date.format(high);
			}
			else{
				dateRange="";
			}
		}
	/**
	 * Returns the date range of an album
	 * 
	 * @return  The date
	 */
		public String getDateRange(){
			return dateRange;
		}		
}
